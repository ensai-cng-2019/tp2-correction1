import requests
from tabulate import tabulate
import properties

URL_EXEMPLE = "https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=statistiques-de-prets-de-dvd-en-2017-cesson-sevigne&rows=1000&apikey=ec567ba33718ba94f57b807c648f5b257cc4af26daf5622f7aff25c7"

if __name__ == '__main__':

    try:

        proxies = {
            # 'http': properties.http_proxy,
            # 'https': properties.https_proxy
        }

        # On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        # On filtre sur les arrêts de Bruz accessible aux personnes à mobilité réduite
        params = {'apikey': properties.api_key,
                  'dataset': "topologie-des-points-darret-de-bus-du-reseau-star",
                  "rows": 1000,
                  'facet': 'codeinseecommune',
                  'facet': 'nomcommune',
                  'facet': 'estaccessiblepmr',
                  'facet': 'mobilier',
                  'refine.estaccessiblepmr': 'true',
                  'refine.nomcommune': 'Bruz'
                  }

        # On envoie une requête poru récupérer les statistiques d'emprunt de films
        response = requests.get(
            'https://data.rennesmetropole.fr/api/records/1.0/search/'
            , proxies=proxies
            , params=params)

        print('response.url')
        print(response.url)

        # Une exception est envoyée si le status de la réponse indique une erreur
        # response.raise_for_status()
        # Retourne None si tout s'est bien passé
        # print('response.raise_for_status: %s' % response.raise_for_status())
        # Ne lève pas d'exception
        print('response.status_code: %s' % response.status_code)
        print("hello")

        # Liste des codes HTTP : https://github.com/psf/requests/blob/master/requests/status_codes.py
        if response.status_code == requests.codes.ok:
            print('ok')
        else:
            print('ko')

        # Exemple avec google
        response = requests.get('https://google.com')
        # response.text si le contenu n'est pas de type json
        print('response.text: %s' % response.text)

        # On récupère la réponse au format json, et dans cette réponse le tableau de résultats (records)
        # print('response.json(): %s' % response.json())
        stops = response.json()['records']

        # On affiche le résultat
        stops_pmr = [
            {'Nom de l arret': stop['fields']['nom'], 
            'Latitude': stop['fields']['coordonnees'][0],
            'Longitude': stop['fields']['coordonnees'][1]} 
            for stop in stops]

        # On affiche le résultat sous forme de tableau avec tabulate
        print(tabulate(stops_pmr, headers="keys", tablefmt='fancy_grid', showindex='always'))

    except requests.exceptions.RequestException as error:
        print(error)
